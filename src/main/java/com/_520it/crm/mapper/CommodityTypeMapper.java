package com._520it.crm.mapper;

import com._520it.crm.domain.CommodityType;
import com._520it.crm.query.CommodityTypeQueryObject;

import java.util.List;

public interface CommodityTypeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CommodityType record);

    CommodityType selectByPrimaryKey(Long id);

    List<CommodityType> selectAll();

    int updateByPrimaryKey(CommodityType record);

    List<CommodityType> listAllType();

    Integer queryTotalCount(CommodityTypeQueryObject qo);

    List<CommodityType> queryData(CommodityTypeQueryObject qo);

    List<CommodityType> selectByParentId(Long id);

    void updateStrat(CommodityType type);
}