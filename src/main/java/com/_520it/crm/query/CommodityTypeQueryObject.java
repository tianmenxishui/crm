package com._520it.crm.query;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

/**
 * Created by 王梦参 on 2017/10/12.
 */
@Getter@Setter
public class CommodityTypeQueryObject extends QueryObject {
    private Long typeId = -1L;
    private String keyword;
    public String getKeyword(){
        return StringUtils.isBlank(keyword)?null:keyword;
    }
}
