package com._520it.crm.web.controller;

import com._520it.crm.domain.CommodityType;
import com._520it.crm.page.AjaxResult;
import com._520it.crm.page.PageResult;
import com._520it.crm.query.CommodityTypeQueryObject;
import com._520it.crm.service.ICommodityTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by 王梦参 on 2017/10/9.
 */
@Controller
@RequestMapping("/commoditytype")
public class CommodityTypeController {
    @Autowired
    private ICommodityTypeService commodityTypeService;

    @RequestMapping
    public String index(){
        return "commoditytype";
    }
    /*
    * 所有分类
    * */
    @RequestMapping("/listAllType")
    @ResponseBody
    public List<CommodityType> listAllType(){
        List<CommodityType> allType = commodityTypeService.listAllType();
        return allType;
    }
    /*
    * 一级分类
    * */
    @RequestMapping("list")
    @ResponseBody
    public PageResult list(CommodityTypeQueryObject qo){
        PageResult query = commodityTypeService.query(qo);
        return query;
    }
    /*
    * 所有类别
    * */
    @RequestMapping("/selectListAlltype")
    @ResponseBody
    public List<CommodityType> selectListAlltype(){
        List<CommodityType> commodityTypes = commodityTypeService.selectAll();
        return commodityTypes;
    }
    /*
    * 新增操作
    * */
    @RequestMapping("/save")
    @ResponseBody
    public AjaxResult save(CommodityType type){
        AjaxResult result = null;
        try {
            commodityTypeService.insert(type);
            result = new AjaxResult("保存成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(e.getMessage());
        }
        return result;
    }
    /*
    * 编辑操作
    * */
    @RequestMapping("/update")
    @ResponseBody
    public AjaxResult update(CommodityType type){
        AjaxResult result = null;
        try {
            commodityTypeService.updateByPrimaryKey(type);
            result = new AjaxResult("修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(e.getMessage());
        }
        return result;
    }
    /*
    * 编辑操作
    * */
    @RequestMapping("/delete")
    @ResponseBody
    public AjaxResult delete(Long id){
        AjaxResult result = null;
        try {
            commodityTypeService.deleteByPrimaryKey(id);
            result = new AjaxResult("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(e.getMessage());
        }
        return result;
    }
    /*
    * 编辑操作
    * */
    @RequestMapping("/updateStrat")
    @ResponseBody
    public AjaxResult updateStrat(CommodityType type){
        AjaxResult result = null;
        try {
            commodityTypeService.updateStrat(type);
            result = new AjaxResult("");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(e.getMessage());
        }
        return result;
    }
}
