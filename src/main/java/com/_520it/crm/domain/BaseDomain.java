package com._520it.crm.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
abstract public class BaseDomain {
	protected Long id;
}
