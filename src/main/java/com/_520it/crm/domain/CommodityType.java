package com._520it.crm.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter@Setter
public class CommodityType extends BaseDomain{
    public static final int NORMAL = 0;//不显示
    public static final int LEAVE = 1;//显示
    /*
    *   类别名称
    * */
    private String text;
    /*
    *   类别类型
    * */
    private String type;

    /*
    *   销售时是否显示
    * */
    private int strat;

    /*
    * 子级菜单
    * */
    private List<CommodityType> children;

    /*
    * 父级菜单
    * */
    private CommodityType parent;

    /*
    * 图标
    * */
    private String iconCls = "icon-add";
}