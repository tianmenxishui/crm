package com._520it.crm.page;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Setter@Getter@AllArgsConstructor
public class PageResult {
	private Integer total;
	private List rows;
}
