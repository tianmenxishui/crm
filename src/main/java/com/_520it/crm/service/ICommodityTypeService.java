package com._520it.crm.service;

import com._520it.crm.domain.CommodityType;
import com._520it.crm.page.PageResult;
import com._520it.crm.query.CommodityTypeQueryObject;

import java.util.List;

/**
 * Created by 王梦参 on 2017/10/9.
 */
public interface ICommodityTypeService {
    int deleteByPrimaryKey(Long id);

    int insert(CommodityType record);

    CommodityType selectByPrimaryKey(Long id);

    List<CommodityType> selectAll();

    int updateByPrimaryKey(CommodityType record);

    List<CommodityType> listAllType();

    PageResult query(CommodityTypeQueryObject qo);

    void updateStrat(CommodityType type);
}
