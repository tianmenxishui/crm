/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50515
Source Host           : localhost:3306
Source Database       : deke

Target Server Type    : MYSQL
Target Server Version : 50515
File Encoding         : 65001

Date: 2017-10-12 20:12:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for commoditytype
-- ----------------------------
DROP TABLE IF EXISTS `commoditytype`;
CREATE TABLE `commoditytype` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `strat` tinyint(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `iconcls` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of commoditytype
-- ----------------------------
INSERT INTO `commoditytype` VALUES ('1', '饮料', '产品类型', '1', null, 'icon-large-yinliao');
INSERT INTO `commoditytype` VALUES ('2', '可乐', '产品类型', '1', '1', 'icon-large-yinliao');
INSERT INTO `commoditytype` VALUES ('3', '外购(香烟)', '产品类型', '1', null, 'icon-large-xiangyan');
INSERT INTO `commoditytype` VALUES ('4', '进口香烟', '产品类型', '1', '3', 'icon-large-xiangyan');
INSERT INTO `commoditytype` VALUES ('13', '雪碧', '产品类型', '0', '1', 'icon-large-yinliao');
INSERT INTO `commoditytype` VALUES ('14', '国内香烟', '产品类型', '0', '3', 'icon-large-xiangyan');
INSERT INTO `commoditytype` VALUES ('16', '油粮副食', '产品类型', '0', null, 'icon-large-youzha');
INSERT INTO `commoditytype` VALUES ('17', '纳豆', '产品类型', '0', '16', 'icon-large-youzha');
