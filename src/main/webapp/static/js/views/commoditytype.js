$(function () {
    var commoditytypeMenu = $("#commoditytype_menu");
    var commoditytypeDatagrid = $("#commoditytype_datagrid");
    var commoditytypDialog = $("#commoditytype_dialog");
    var commoditytypeForm = $("#commoditytype_form");
    var searchBtn = $("#searchBtn");
    var typeId;
    /*
    * 所有分类菜单栏
    * */
    commoditytypeMenu.tree({
        animate:true,
        url:"/commoditytype/listAllType",
        onDblClick:function (node) {
            typeId = node.id;
            commoditytypeDatagrid.datagrid("reload");
        }
    });
    /*
    * 类型表格
    * */
    commoditytypeDatagrid.datagrid({
        fit:true,
        rownumbers:true,
        singleSelect:true,
        pagination:true,
        url:'/commoditytype/list',
        fitColumns:true,
        toolbar:'#commoditytype_datagrid_tb',
        striped:true,
        columns:[
            [
                {field:'text',align:'center',width:10,title:'类别名称'},
                {field:'type',align:'center',width:10,title:'类别类型'},
                {field:'strat',align:'center',width:10,title:'销售时是否显示',formatter:strat,editor:{type:'checkbox',options:{on:'P',off:''}}},
            ]
        ],
        onBeforeLoad:function (param) {
            param.typeId = typeId;
            return true;
        },
        onClickCell:function(index,field,value){
            commoditytypeDatagrid.datagrid("selectRow",index)
            var rowData = commoditytypeDatagrid.datagrid("getSelected");
            if(field=="strat"){
                $.get("/commoditytype/updateStrat",{"id":rowData.id,"strat":value},function (data) {
                    if(data.success){
                        //提示消息,当点确定的时候,关闭对话框,刷新数据表格
                        commoditytypeDatagrid.datagrid("reload");
                    }else{
                        $.messager.alert("温馨提示",data.msg,"error");
                    }
                })
            }
        }
    });
    /*
    * 对话框
    * */
    commoditytypDialog.dialog({
        width:320,
        height:280,
        buttons:'#commoditytype_dialog_bt',
        closed:true
    });
    //对按钮进行统一事件监听
    $("a[data-cmd]").on("click",function(){
        var cmd = $(this).data("cmd");
        if(cmd){
            cmdObj[cmd]();
        }
    });
    /*
     * 高级查询
     * */
        searchBtn.textbox({
            width:230,
            label:"关键字:",
            labelWidth:50,
            prompt:"请输入搜索关键字",
            buttonText:'搜索',
            buttonIcon:'icon-search',
            onClickButton:function(){
                var keyword = $(this).val();
                commoditytypeDatagrid.datagrid("load",{
                    keyword:keyword
                });
            }
        });
    //方法统一管理起来]
    var cmdObj = {
        add:function(){
            //1.清空表单数据
            commoditytypeForm.form("clear");
            //2.设置对话框的标题
            commoditytypDialog.dialog("setTitle","新增");
            //3.打开对话框
            commoditytypDialog.dialog("open");
        },
        edit:function(){
            var rowData = commoditytypeDatagrid.datagrid("getSelected");
            if(rowData){
                //1.清空表单数据
                commoditytypeForm.form("clear");
                //2.设置对话框的标题
                commoditytypDialog.dialog("setTitle","编辑");
                //3.打开对话框
                commoditytypDialog.dialog("open");
                /*
                * 编辑时禁用上级类型
                * */
                $("#typeId").combobox("disable");
                /*
                * 回显数据
                * */
                if (rowData.parent){
                    rowData['parent.id'] = rowData.parent.id;
                }
                commoditytypeForm.form("load",rowData);
            }else{
                $.messager.alert("温馨提示","请选这需要编辑的行","warning");
            }
        },
        remove:function(){
            var rowData = commoditytypeDatagrid.datagrid("getSelected");
            if(rowData){
                $.messager.confirm("温馨提示","您确定要删除这条数据吗",function (yes) {
                    $.post("/commoditytype/delete",{"id":rowData.id},function (data) {
                        if(data.success){
                            commoditytypeMenu.tree("reload");
                            commoditytypeDatagrid.datagrid("reload");
                            $.messager.alert("温馨提示",data.msg,"info")
                        }else{
                            $.messager.alert("温馨提示",data.msg,"error");
                        }
                    })
                });
            }else{
                $.messager.alert("温馨提示","请选这需要编辑的行","warning");
            }
        },
        reload:function(){
            commoditytypeMenu.tree("reload");
            commoditytypeDatagrid.datagrid("reload");
        },
        save:function(){
            var formId = $("[name=id]").val();
            var url;
            if(formId){
                url = "/commoditytype/update";
            }else{
                url = "/commoditytype/save";
            }
            console.log(url);
            commoditytypeForm.form("submit",{
                url:url,
                onSubmit:function (param) {
                     param['parent.id'] =$("#typeId").combo("getValue",function (node) {
                         return node.id;
                     })
                    return true;
                },
                success:function (data) {
                    data = $.parseJSON(data);
                    if(data.success){
                        //提示消息,当点确定的时候,关闭对话框,刷新数据表格
                        commoditytypeMenu.tree("reload");
                        commoditytypeDatagrid.datagrid("reload");
                        $.messager.alert("温馨提示",data.msg,"info",function(){
                            commoditytypDialog.dialog("close");
                        });
                    }else{
                        $.messager.alert("温馨提示",data.msg,"error");
                    }
                }
            });
        },
        cancel:function(){
            commoditytypDialog.dialog("close");
        },
    };
    $("#checkboxId").click(function() {
        alert("1");
        if ($(this).attr("checked") == true) {
            console.log("选中");
        } else {
            console.log("没有选中");
        }
    });
});
function strat(value,record,index) {
    if(value==0){
        return "<font color='green'>销售时不显示</font>";
    }else if(value==1){
        return "<font color='red'>销售时显示</font>";
    }
}