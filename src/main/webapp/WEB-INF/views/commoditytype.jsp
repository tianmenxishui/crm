<%--
  Created by IntelliJ IDEA.
  User: 王梦参
  Date: 2017/10/9
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>商品类别列表</title>
    <%@include file="common.jsp" %>
    <script type="text/javascript" src="/static/js/views/commoditytype.js"></script>
</head>
<body>
    <div  class="easyui-layout" fit="true">
        <div data-options="region:'west',title:'所有类别'" style="width:180px">
            <!-- 菜单树 -->
            <ul id="commoditytype_menu"></ul>
        </div>
        <div data-options="region:'center',border:false">
            <table id="commoditytype_datagrid"></table>
        </div>
    </div>
    <div id="commoditytype_datagrid_tb">
        <div>
            <a class="easyui-linkbutton" iconCls="icon-add" plain="true" data-cmd="add">新增</a>
            <a id="commoditytype_editBtn" class="easyui-linkbutton" iconCls="icon-edit" plain="true"  data-cmd="edit">编辑</a>
            <a id="commoditytype_quitBtn" class="easyui-linkbutton" iconCls="icon-remove" plain="true" data-cmd="remove">删除</a>
            <a class="easyui-linkbutton" iconCls="icon-reload" plain="true" data-cmd="reload">刷新</a>
        </div>
        <div>
            <input id="searchBtn" type="text">
        </div>
    </div>
    <!-- 定义对话框 -->
    <div id="commoditytype_dialog">
        <form id="commoditytype_form" method="post">
            <input type="hidden" name="id">
            <div align="center" style="margin-top: 20px;" >
                <div>
                    <select name="type"  id="type_mold" class="easyui-combobox"
                             data-options="
						 width:200,
						 height:50,
						 label:'分类类型:',
						 labelPosition:'top',
						 valueField:'type',
						 textField:'name',
						 value:'请选择类型',
						 url:'/static/js/data/type.json'
						">
                    </select>
                </div>
                <div>
                    <select name="parent.id"  id="typeId" class="easyui-combobox"
                             data-options="
						 width:200,
						 height:50,
						 label:'一级类别:',
						 labelPosition:'top',
						 valueField:'id',
						 textField:'text',
						 value:'一级类别',
						 url:'/commoditytype/selectListAlltype'
						">
                    </select>
                </div>
                <div>
                    <input type="text" name="text" class="easyui-textbox" data-options="label:'类别名称:',labelPosition:'top', width:200,
						 height:50,">
                </div>
            </div>
        </form>
    </div>
    <!-- 定义对话框底部按钮 -->
    <div id="commoditytype_dialog_bt">
        <a class="easyui-linkbutton" iconCls="icon-save" plain="true" data-cmd="save">保存</a>
        <a class="easyui-linkbutton" iconCls="icon-cancel" plain="true" data-cmd="cancel">取消</a>
    </div>
</body>
</html>
